import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { SafeAreaView } from 'react-navigation';

import ChatTradeSummary from '../../components/ChatTradeSummary';
import withChatKit from '../../components/withChatKit';
import ChatActions from '../../components/ChatActions';
import { GiftedChat } from 'react-native-gifted-chat';
import Axios from 'axios';

import { EOS_API_HOST, EOS_API_PORT } from 'react-native-dotenv';

const status = {
  READY: 'READY',
  IN_ESCROW: 'IN_ESCROW',
  PAYMENT_SENT: 'PAYMENT_SENT',
  PAYMENT_RECEIVED: 'PAYMENT_RECEIVED'
}
class ChatCore extends Component {
  constructor(props){
    super(props);

    this.state = {
      connected: false,
      messages: [],
      chatConnection: null,
      status: 'READY'
    }

    this.transactURL = `http://${EOS_API_HOST}:${EOS_API_PORT}/transact`;

    this.sendMessage = this.sendMessage.bind(this);
    this.createGiftedMessage = this.createGiftedMessage.bind(this);
    this.loadChat = this.loadChat.bind(this);
    this._ready = this._ready.bind(this);
    this._inEscrow = this._inEscrow.bind(this);
    this._paymentSent = this._paymentSent.bind(this);
    this._paymentReceived = this._paymentReceived.bind(this);
  }

  componentDidMount() {
    if ( this.props.mode === 'sell') {
      Axios.get(`${this.statusURL}/reset`)
      .then( res => {
        console.log('reset status', res.data);
      })
      .catch( e=> {
        console.log(e);
      })
    }
  }

  componentDidUpdate() {
    if ( this.props.chatManager && !this.state.connected ) {
      this.props.chatManager.connect()
      .then( user => {
        console.log(user);
        user.subscribeToRoom({
          roomId: 9134662,
          hooks: {
            onNewMessage: message => {
              console.log(message);
              this.setState(pS => ({
                messages: GiftedChat.append(pS.messages, [this.createGiftedMessage(message)]),
              }))
            }
          },
          messageLimit: 0
        });
        this.setState({ chatConnection: user, connected: true });
      })
      .catch( e => {
        this.setState({ error: e.message })
      })
    }
  }

  createGiftedMessage(message) {
    // const self = this.props.navigation.getParam('self');
    // const partner = this.props.navigation.getParam('partner');
    
    let avatar = null;
    // avatar =  self.name === message.senderId ? self.img : partner.img;
    return {
      _id: message.id,
      text: message.text,
      createdAt: message.createdAt,
      user: {
        _id: message.senderId,
        avatar: avatar,
      }
    }
  }

  // createGiftedSystemMessage(Component){
  //   return {
  //     _id: 1,
  //     text: message,
  //     createdAt: new Date(Date.UTC()),
  //     system: true,
  //     // Any additional custom parameters are passed through
  //   }
  // }

  sendMessage(messages) {
    messages.forEach( message => {
      this.state.chatConnection.sendMessage({
        text: message.text,
        roomId: this.state.chatConnection.rooms[0].id
      });
    })
  }

  loadChat() {
    if ( !this.state.connected ) {
      return (
        <Text> Loading chat... </Text>
      )
    }
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.sendMessage(messages)}
        user={{
          _id: this.props.navigation.getParam('self').name,
          name: this.props.navigation.getParam('self').name
        }}
        showUserAvatar={ true }
      /> 
    )
  }

  _ready(){
    console.log('ready callback');
    if ( this.props.navigation.getParam('mode') === 'sell' ){
      console.log('sending fund escrow trans');
      Axios.post( this.transactURL, {
        contract: 'esceos',
	      action: 'lock',
      	name: 'seller',
	      authKey: '5JrsF5F63BMoTcJwBo7is4t4XPRpPbDyn8en9p7pzgfAJY74HkF',
        args: {
          buyer: 'buyer',
          seller: 'seller',
          quantity: '4.00 ONO'
        }
      })
      .then( result => {
        console.log(result);
      })
      .catch( e=> {
        console.log(e);
      })
    } else {

    }
  }

  _inEscrow(){
    if ( this.props.navigation.getParam('mode') === 'buy' ){
      console.log('sending fund escrow trans');
      Axios.post( this.transactURL, {
        contract: 'esceos',
        action: 'buyerok',
        name: 'buyer',
        authKey: '5JU5YYSFYgZGHZNhRnjeDgu7VpC6frdzFqz2JtE6f1TngPodYSF',
        args: {
          buyer: 'buyer',
          seller: 'seller'
        }
      })
      .then( result => {
        console.log(result);
      })
      .catch( e=> {
        console.log(e);
      })
    } else {

    }
  }

  _paymentSent() {
    if ( this.props.navigation.getParam('mode') === 'sell' ){
      console.log('sending fund escrow trans');
      Axios.post( this.transactURL, {
        contract: 'esceos',
        action: 'sellerok',
        name: 'seller',
        authKey: '5JrsF5F63BMoTcJwBo7is4t4XPRpPbDyn8en9p7pzgfAJY74HkF',
        args: {
          buyer: 'buyer',
          seller: 'seller'
        }
      })
      .then( result => {
        console.log(result);
      })
      .catch( e=> {
        console.log(e);
      })
    } else {

    }
  }

  _paymentReceived() {
    this.props.navigation.navigate('TradesList');
  }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#FFF'
        }}
      >
        <ChatTradeSummary
          partner={this.props.navigation.getParam('partner')} 
          self={this.props.navigation.getParam('self')} 
        />
        <ChatActions
          callbacks={{
            READY: this._ready,
            IN_ESCROW: this._inEscrow,
            PAYMENT_SENT: this._paymentSent,
            PAYMENT_RECEIVED: this._paymentReceived
          }}
          mode={this.props.navigation.getParam('mode')}
        />
        { this.loadChat() }
      </SafeAreaView>
    )
  }
}

export default withChatKit(ChatCore);