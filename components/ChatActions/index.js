import React, { Component } from 'react';
import { Text, Dimensions } from 'react-native';
import Button from 'react-native-button';
import { SafeAreaView } from 'react-navigation';
import Axios from 'axios';

// env
import { EOS_API_HOST, EOS_API_PORT } from 'react-native-dotenv';

const status = {
  READY: 'READY',
  IN_ESCROW: 'IN_ESCROW',
  PAYMENT_SENT: 'PAYMENT_SENT',
  PAYMENT_RECEIVED: 'PAYMENT_RECEIVED'
}

export default class ChatActions extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      status: 'READY'
    }

    this.statusURL = `http://${EOS_API_HOST}:${EOS_API_PORT}`;

    this.handleAction = this.handleAction.bind(this);
    this.getButtonText = this.getButtonText.bind(this);
    this.checkStatus = this.checkStatus.bind(this);
    this.updateStatus = this.updateStatus.bind(this);
  }

  componentDidMount() {

    if ( this.props.mode === 'sell') {
      Axios.get(`${this.statusURL}/reset`)
      .then( res => {
        console.log('reset status', res.data);
      })
      .catch( e=> {
        console.log(e);
      })
    }
    setInterval( this.checkStatus, 5000);
  }

  checkStatus() {
    Axios.get(`${this.statusURL}/status`)
    .then( res => {
      if ( res.data !== this.state.status ) {
        this.setState({status: res.data })
        console.log('updating status');
      }
    })
    .catch( e=> {
      console.log(e);
    })
  }

  updateStatus(oldStatus,newStatus) {
    Axios.get(`${this.statusURL}/statusupdate?s=${newStatus}`)
    .then( res => {
      console.log('status is', res.data);
    })
    .catch( e=> {
      console.log(e);
    })
    this.props.callbacks[oldStatus]();
  }

  

  getButtonText(){
    console.log(this.props);
    if ( this.props.mode === 'sell' ){
      switch (this.state.status) {
        case 'READY': 
          return 'Fund escrow';
        case 'IN_ESCROW':
          return 'Wait for buyer';
        case 'PAYMENT_SENT': 
          return 'Confirm receipt';
        default:
          return 'Complete transaction';
      }
    } else {
      switch (this.state.status) {
        case 'READY': 
          return 'Waiting for seller escrow';
        case 'IN_ESCROW':
          return 'Confirm payment sent';
        case 'PAYMENT_SENT': 
          return 'Wait for seller to confirm receipt';
        case 'PAYMENT_RECEIVED':
          return 'Complete transactions'
        default:
          return 'something is broken';
      }
    }
  }

  handleAction(e) {
    e.preventDefault();
    switch (this.state.status) {
      case 'READY':
        this.props.callbacks.READY(); 
        if ( this.props.mode === 'sell') {
          this.updateStatus('READY','IN_ESCROW');
        }
        break;
      case 'IN_ESCROW':
        if ( this.props.mode === 'buy') {
          this.updateStatus('IN_ESCROW','PAYMENT_SENT');
        }
        break;
      case 'PAYMENT_SENT':
        if ( this.props.mode === 'sell') {
          this.updateStatus('PAYMENT_SENT','PAYMENT_RECEIVED');
        }
        break;
      case 'PAYMENT_RECEIVED':
        this.props.callbacks.PAYMENT_RECEIVED();
      default:
        return 'something is broken';
    }
  }

  render () {
    const { height, width } = Dimensions.get('window');
    
    return (
      <SafeAreaView
        style={{
          height: 70,
          width: width,
          paddingTop: 20,
          paddingBottom: 20,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          shadowOffset:{  width: 0,  height: 5,  },
          shadowColor: '#999',
          shadowOpacity: .5,
        }}
      >
        <Button
          containerStyle={{
            backgroundColor: '#5A02D1',
            borderRadius: 30,
            paddingLeft: 30,
            paddingRight: 30,
            paddingTop: 15,
            height: 50
          }}
          style={{
            color: '#FFF',
            fontWeight: '900'
          }}
          onPress={(e)=>( this.handleAction(e) )}
        >
          { this.getButtonText() }
        </Button>
      </SafeAreaView>
    )
  }
}