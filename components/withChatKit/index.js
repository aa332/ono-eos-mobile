// library dependencies
import React,  { Component } from 'react';
import { ChatManager, TokenProvider } from '@pusher/chatkit/react-native'


const withChatKit = (WComponent) => {
  return class ChatKit extends Component {
    constructor(props) {
      super(props);
      this.state = { 
        chatManager: null 
      };
    }

    componentDidMount() {
      this.setState({
        chatManager: new ChatManager({
          instanceLocator: 'v1:us1:9da80da9-6056-46cf-b834-b6f4e422072f',
          userId: this.props.navigation.getParam('self').name,
          tokenProvider: new TokenProvider({ url: 'https://us1.pusherplatform.io/services/chatkit_token_provider/v1/9da80da9-6056-46cf-b834-b6f4e422072f/token' })
        })
      }) 
    }

    render() {
      return (
        <WComponent chatManager={this.state.chatManager} {...this.props} />
      )
    }
  }
}

export default withChatKit;