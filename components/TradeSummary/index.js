import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native'
import { Avatar } from 'react-native-elements';

export default class TradeSummary extends Component {
  render() {
    const { height, width } = Dimensions.get('window');
    return (
      <View
        style={{
          height: 90,
          width: width,
          flexDirection: 'column',
          alignItems: 'center',
          backgroundColor: '#FFF'
        }}
      >
        <Text
          style={{
            fontSize: 18,
            fontWeight: '600',
            paddingTop: 20
          }}
        >
          { this.props.mode === 'sell' ? 'Sell currency to' : 'Buy currency from' }
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            padding: 10
          }}
        >
          <Avatar
            small
            rounded
            source={{uri: this.props.user.img}}
            activeOpacity={1.0}
          />
          <Text
            style={{
              fontSize: 22,
              fontWeight: '700',
              paddingLeft: 20,
              paddingRight: 20,
            }}
          >
            {this.props.user.name}
          </Text>
        </View>
      </View>
    )
  }
}