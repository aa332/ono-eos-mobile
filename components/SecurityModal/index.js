import React, {Component} from 'react';
import {Text, TouchableHighlight, View, StyleSheet} from 'react-native';
import Expo, { Constants } from 'expo';
import Modal from "react-native-modal";

export default class SecurityModal extends Component {
  constructor(props) {
    super(props);  

    this.state = {
      compatible: false,
      fingerprints: false,
      result: ''
    }
  }

  componentDidMount() {
    this.checkDeviceForHardware();
    this.checkForFingerprints();
    if (compatible && fingerprints) {
        this.scanFingerprint();
    }  
  }
  
  checkDeviceForHardware = async () => {
    let compatible = await Expo.Fingerprint.hasHardwareAsync();
    this.setState({compatible})
  }
  
  checkForFingerprints = async () => {
    let fingerprints = await Expo.Fingerprint.isEnrolledAsync();
    this.setState({fingerprints})
  }  
  
  cancel() {
    Expo.Fingerprint.cancelAuthenticate();
    this.props.hideModal(); 
  }
  
  scanFingerprint = async () => {        
    let result = await Expo.Fingerprint.authenticateAsync('Scan your finger.');    
    if (result.success) {
        this.props.onSuccess();
    }
    else {
        this.props.onFail();
    }
  }
    
  render() {
    return (
      <Modal           
        onBackdropPress={() => this.cancel()}
        isVisible={true}
      >                     
        <View style={{ backgroundColor: '#FFF' }} >
          <Text>Security modal</Text>
        </View>
      </Modal>
    );
  }
}