import React, { Component } from 'react';
import { Text, Dimensions } from 'react-native';
import { SafeAreaView } from 'react-navigation';

export default class ExchangeRate extends Component {

  render() {
    let { balance } = this.props;
    if ( balance ) {
      try {
        balance =   balance.slice(0,-3);
        balance = balance * 14;
        balance = `${balance}.00`
      } catch (e) {
        balance = 0;
      }
    }
    return (
      <SafeAreaView
        style={{
          height: 25,
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center'
        }}
      >
        <Text
          style={{
            fontSize: 16,
            fontWeight: '700',
            paddingLeft: 10,
            paddingRight: 10
          }}
        >
          ¥ { balance }
        </Text>
        <Text
          style={{
            fontSize: 16,
            fontWeight: '700',
            color: '#3AEFB0',
            paddingLeft: 10,
            paddingRight: 10
          }}
        >
          UP 300%
        </Text>
      </SafeAreaView>
    );
  }
}