import React, { Component } from 'react';
import { SafeAreaView } from 'react-navigation';
import { Text } from 'react-native';

export default class ChatTradeSummary extends Component {
  render() {
    console.log('ChatTradeSummary', this.props);
    return (
      <SafeAreaView
        style={{
          height: 50,
          backgroundColor: '#FFF',
          shadowOffset:{  width: 0,  height: 5,  },
          shadowColor: '#999',
          shadowOpacity: .5,
          backgroundColor: '#FFF',
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <Text
          style={{
            fontSize: 18,
            fontWeight: '900'
          }}
        >
          {this.props.self.offer} ONO 
        </Text>
        <Text
          style={{
            fontSize: 18,
            fontWeight: '900'
          }}
        >
         &nbsp;>&nbsp; 
        </Text>
        <Text
          style={{
            fontSize: 18,
            fontWeight: '900',
            color: '#0064FF'
          }}
        >
          ¥ {this.props.partner.offer}
        </Text>
      </SafeAreaView>
    )
  }
}