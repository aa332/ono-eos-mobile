import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableWithoutFeedback } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { Avatar } from 'react-native-elements';

export default class TradeItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      img: null
    }
  
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if ( nextProps.mode === 'sell' ) {
      return {
        img: nextProps.users['buy'].img
      }
    }
    return {
      img: nextProps.users['sell'].img
    }
  }

  render() {
    const { height, width } = Dimensions.get('window');
    const user = this.props.mode === 'sell' ? this.props.users['buy'] : this.props.users['sell']; 
    const offer = this.props.mode === 'sell' ? `¥ ${user.offer}` : `${user.offer} ONO`; 
    console.log(user);
    return (
      <TouchableWithoutFeedback
        onPress={() => ( this.props.goToTrade(this.props.mode) )}
      >
        <View 
          style={{
            padding: 20,
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            width: width,
            height: 90
          }}
        >
          <View
            style={{
              width: width * 0.3,
              height: 90,
              alignItems: 'center',
              justifyContent: 'flex-start',
              flexDirection: 'row',
              paddingLeft: width * 0.05
            }}
          >
            <Avatar
              medium
              rounded
              source={{uri: user.img}}
              activeOpacity={1.0}
            />
          </View>
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems:'flex-start',
              height: 90,
              width: width * 0.4
            }}
          >
            <Text
              style={{
                fontSize:18,
                fontWeight: '700'
              }}
            >
              { user.name }
            </Text>
            <Text
              style={{
                fontSize: 16,
                fontWeight: '400',
                color: '#CCC'
              }}
            >
              {user.trades} trades
            </Text>
          </View>
          <View
            style={{
              width: width * 0.3,
              height: 90,
              flexDirection: 'row',
              alignItems: 'center'
            }}
          >
            <Text
              style={{
                fontSize: 22,
                color: '#3A5AE4'
              }}
            >
              { offer } 
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}